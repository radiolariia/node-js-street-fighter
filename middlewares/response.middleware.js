const responseMiddleware = (req, res, next) => {
   // TODO: Implement middleware that returns result of the query
    if(res) {
        res.status(200).send(res);
    } else {
        res.status(404).send({
            error: true,
            message: ''
        });
    }
    next();
}

exports.responseMiddleware = responseMiddleware;