class User {
    constructor({ firstName, lastName, email, phoneNumber, password }) {
        // id = '',
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.phoneNumber = phoneNumber;
    this.password = password; // min 3 symbols
    }
}

module.exports.User = User;