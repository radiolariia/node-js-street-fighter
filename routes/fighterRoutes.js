const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');
const { Fighter } = require('../models/fighter');

const router = Router();

// TODO: Implement route controllers for fighter

// FIGHTER
//         GET /api/fighters
//         GET /api/fighters/:id
//         POST /api/fighters
//         PUT /api/fighters/:id
//         DELETE /api/fighters/:id
router.get('/', (req, res, next) => {
    const fighters = FighterService.getFighters();

    if (fighters.length > 0) {
        return res.status(200).json(fighters);
    } else {
        return res.status(404).json({
            error: true,  
            message: 'There are no fighters'
        });
    }
})
router.get(`/:id`, (req, res, next) => {
    const id = req.params.id;
    const fighter = FighterService.getOneFighter({ id });

    if(fighter) {
        return res.status(200).json(fighter);
    } else {
        return res.status(404).json({
            error: true,  
            message: 'Looks like any fighter has not such id'
        });
    }
})
router.post('/', createFighterValid, (req, res, next) => {
    const newFighter = new Fighter(req.body);
    const result = FighterService.create(newFighter);

    if(result) {
        return res.status(200).json(result);
    } else {
        return res.status(400).json({
            error: true,  
            message:'Fighter with such name already exists!'
        });
    }
})
router.put(`/:id`, updateFighterValid, (req, res, next) => {
    const id = req.params.id;
    const fighterData = req.body;
    const updatedFighter = FighterService.update(id, fighterData);

    if (updatedFighter) {
        return res.status(200).json(updatedFighter);
    } else {
        return res.status(404).json({
            error: true,  
            message: 'Such fighter does not exist OR you are trying to type name of the existing fighter'
        });
    }
})
router.delete(`/:id`, (req, res, next) => {
    const id = req.params.id;
    const fighterToDelete = FighterService.delete(id);

    if (fighterToDelete) {
        return res.status(200).json('Deleted successfully!');
    } else {
        return res.status(404).json({
            error: true,  
            message: 'Such fighter does NOT exist'
        });
    }
})

module.exports = router;